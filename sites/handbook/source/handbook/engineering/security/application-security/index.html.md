---
layout: handbook-page-toc
title: "Application Security"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Application Security Mission

The application security team's mission is to support the business and ensure that all GitLab products securely manage customer data. We do this by working closely with both engineering and product teams.

## Application Security Roadmap

Please see the [Security Engineering and Research Program Strategy document][9].

## Roles & Responsbilities

Please see the [Application Security Job Family page][6].

## Application Security KPIs & Other Metrics in Sisense

* For KPIs and other metrics, please [see this page][7].
* For Embedded KPIs which you filter by section, stage, or group, please [see this page][8].

## General Role Functions

### Stable Counterparts

Please see the [Application Security Stable Counterparts page][4].

### Application Security Reviews

Please see the [Application Security Reviews page][1].

## Project ownership

Please see the [Appsec project owners page][2]

## Application Security Engineer Runbooks

Please see the [Application Security Engineer Runbooks page index][5]

## Meeting Recordings

The following recordings are available internally only:

* [AppSec Sync](https://drive.google.com/drive/folders/1sxnBhPNDofWg5JmKqrhEl5y4_aWldTbt)
* [AppSec Leadership Weekly](https://drive.google.com/drive/folders/1jyNYP2AOqoOPqr4qGMuh7PGha_j-7brb)

[1]: /handbook/engineering/security/application-security/appsec-reviews.html
[2]: /handbook/engineering/security/application-security/project-owners.html
[3]: /handbook/engineering/security/application-security/vulnerability-management.html
[4]: /handbook/engineering/security/application-security/stable-counterparts.html
[5]: /handbook/engineering/security/application-security/runbooks
[6]: /job-families/engineering/application-security/index.html
[7]: https://app.periscopedata.com/app/gitlab/641782/Appsec-hackerone-vulnerability-metrics
[8]: https://app.periscopedata.com/app/gitlab/758795/Appsec-Embedded-Dashboard
[9]: https://docs.google.com/document/d/1Mba9ZhuVr2qBkvR7AqzNTUFMUTapJqiXkPUqc9Gr8io/edit

## Backlog reviews

When necessary a backlog review can be initiated, please see the [Vulnerability Management Page][3] for more details.
